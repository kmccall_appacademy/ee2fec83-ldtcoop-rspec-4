class Timer
  def initialize
    @timer = 0
  end

  def seconds
    @timer
  end

  def seconds=(num)
    @timer = num
  end

  def time_string
    time_hash = { hours: 0, minutes: 0, seconds: @timer }
    # seconds to minutes
    if time_hash[:seconds] > 60
      time_hash[:minutes] = time_hash[:seconds] / 60
      time_hash[:seconds] %= 60
    end
    # minutes to hours
    if time_hash[:minutes] > 60
      time_hash[:hours] = time_hash[:minutes] / 60
      time_hash[:minutes] %= 60
    end
    # format time_hash values
    time_hash.each do |k, v|
      str = v.to_s
      str = "0#{str}" if str.length < 2
      time_hash[k] = str
    end
    "#{time_hash[:hours]}:#{time_hash[:minutes]}:#{time_hash[:seconds]}"
  end
end
