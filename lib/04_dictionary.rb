class Dictionary
  # TODO: your code goes here!
  def initialize
    @d = {}
  end

  def entries
    @d
  end

  def keywords
    @d.keys.sort
  end

  def add(opt)
    if opt.is_a? Hash
      @d = @d.merge(opt)
    else
      @d[opt] = nil
    end
  end

  def include?(elem)
    @d.key?(elem)
  end

  def find(elem)
    @d.select do |k, _v|
      k.include?(elem)
    end
  end

  def printable
    definitions = ""
    self.keywords.each_with_index do |el, ind|
      definitions += %Q{[#{el}] "#{@d[el]}"}
      definitions += "\n" unless ind == @d.length - 1
    end
    definitions
  end
end
