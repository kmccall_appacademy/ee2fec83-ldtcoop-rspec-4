# # Hints
#
# Remember that one degree fahrenheit is 5/9 of one degree celsius,
# and that the freezing point of water is 0 degrees celsius but 32
# degrees fahrenheit.
#
# Remember to define the `from_celsius` factory method as a *class*
# method, not an *instance* method.
#
# The temperature object's constructor should accept an *options hash*
# which contains either a `:celcius` entry or a `:fahrenheit` entry.


class Temperature
  # TODO: your code goes here!
  def initialize(opts = {})
    @system = :f if opts.key?(:f)
    @system = :c if opts.key?(:c)
    @temp = opts[@system]
  end

  attr_accessor :system, :temp

  def in_fahrenheit
    if @system == :f
      @temp
    else
      (@temp * 1.8) + 32
    end
  end

  def in_celsius
    if @system == :c
      @temp
    else
      (@temp - 32).to_f * (5.0 / 9)
    end
  end

  def self.from_celsius(degrees)
    Temperature.new(c: degrees)
  end

  def self.from_fahrenheit(degrees)
    Temperature.new(f: degrees)
  end
end

class Celsius < Temperature
  def initialize(degs)
    @system = :c
    @temp = degs
  end
end

class Fahrenheit < Temperature
  def initialize(degs)
    @system = :f
    @temp = degs
  end
end
