class Book
  # TODO: your code goes here!
  def initialize
    @book
  end

  def title=(name)
    articles = ["the", "a", "an"]
    conjunctions = ["and"] # more could be added as needed
    prepositions = ["to", "in", "of"] # more could be added as needed
    fixed_title = ""
    arr = name.split(" ")
    arr.each_with_index do |el, ind|
      if ind == 0
        fixed_title += el.capitalize
      elsif articles.include?(el)
        fixed_title += el
      elsif conjunctions.include?(el)
        fixed_title += el
      elsif prepositions.include?(el)
        fixed_title += el
      else
        fixed_title += el.capitalize
      end
      fixed_title += " " unless ind == arr.length - 1
    end 
      @book = fixed_title
  end

  def title
    @book
  end

  private


end
